// Write a menu driven program to calculate volume of the box.Provide neceesary
// constructors.

#include "iostream"

using namespace std;

enum option{EXIT,GETDATA,RESULT};

class Volume
{
	int length;
	int width;
	int height;

public:
	Volume();

	void getData();

	int result();
};

Volume :: Volume()
{
	length = 0;
	width = 0;
	height = 0;
}

void Volume::getData()
{
	cout<<"Enter Length"<<endl;
	cin>>length;

	cout<<"Enter Width"<<endl;
	cin>>width;

	cout<<"Enter Height"<<endl;
	cin>>height;
}

int Volume::result()
{
	return length*height*width;
}

int getChoice()
{
	int ch;
	cout<<"0. EXIT\n1. GET DATA\n2. RESULT\n"<<endl;
	cin>>ch;

	return ch;
}

int main()
{
	Volume v;

	int ch;

	while(ch=getChoice())
	{
		switch(ch)
		{
			case GETDATA:
				v.getData();
				break;
			case RESULT:
				cout<<"RESULT : "<<v.result()<<endl;
				break;
		}
	}
}