#include "iostream"

using namespace std;

enum option{EXIT,PAYING_CAR,NON_PAYING_CAR,DISPLAY};

class tollBooth
{
private:
	// THIS IS FOR DATA TYPES 
	double amt;
	int npcar;
	int car;
public:
	// THIS IS FOR MEMBER FUNCTIONS
	tollBooth();
	void nonPayingCar();
	void payingCar();
	void display();
};


// This is for member function defination
tollBooth::tollBooth()
{
	amt = 0;
	npcar = 0;
	car = 0;
}

void tollBooth::nonPayingCar()
{
	npcar++;
}

void tollBooth::payingCar()
{
	car++;
	amt = amt + 0.50;
}

void tollBooth::display()
{
	cout<<"*********************************************"<<endl;
	cout<<"TOTAL PAYING CARS : "<<car<<endl;

	cout<<"TOTAL NON PAYING CARS : "<<npcar<<endl;

	cout<<"TOTAL AMOUNT COLLECTED : "<<amt<<endl;
	cout<<"*********************************************"<<endl;
}

int getOption()
{
	int ch;
	cout<<"0. EXIT\n1. PAYING CAR\n2. NON PAYING CAR\n3. DISPLAY"<<endl;
	cin>>ch;

	return ch;
}
////////////////////////////////////////////////
int main()
{
	int ch;
	tollBooth t;

	while(ch=getOption())
	{
		switch(ch)
		{
			case PAYING_CAR:
					t.payingCar();
					break;
			case NON_PAYING_CAR:
					t.nonPayingCar();
					break;
			case DISPLAY:
					t.display();
					break;
		}
	}
}