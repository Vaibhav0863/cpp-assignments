#ifndef CYLINDER_H_
#define CYLINDER_H_

class Volume
{
	int radius;
	int height;

public:
	Volume();
	void getData();
	double display();
};

#endif