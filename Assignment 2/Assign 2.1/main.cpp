#include "iostream"
#include "includes/cylinder.h"
#include "function.cpp"

using namespace std;

enum option
{
	EXIT,
	GETDATA,
	DISPLAY
};

int getChoice()
{
	int ch;

	cout<<"0. EXIT\n1. GET DATA\n2. DISPLAY"<<endl;
	cin>>ch;

	return ch;
}

int main()
{
	Volume v;
	int ch;

	while(ch=getChoice())
	{
		switch(ch)
		{
			case GETDATA:
					v.getData();
					break;
			case DISPLAY:
					cout<<"volume of Cylinder : "<<v.display()<<endl;
					break;
		}
	}
}