#include "iostream"
#include "cstdlib"

using namespace std;

enum option
{
	EXIT,
	ACCEPT,
	PRINT,
	SEARCH,
	SORT
};

class Student
{
string name;
char gender;
int rollnumber;
double marks;

public:
	void accept();
	void print();
	int search(int flag);
	int getRollNumber();
	// void sort();

};

void Student::accept()
{
	cout<<"**************************************"<<endl;
	cout<<"Enter Name : "<<endl;
	cin>>name;

	cout<<"Enter Gender : "<<endl;
	cin>>gender;

	cout<<"Enter RollNumber : "<<endl;
	cin>>rollnumber;

	cout<<"Enter Marks : "<<endl;
	cin>>marks;
	cout<<"**************************************"<<endl;
}

void Student::print()
{
	cout<<"***************************************"<<endl;
	cout<<"Name : "<<name<<endl;
	cout<<"Gender : "<<gender<<endl;
	cout<<"RollNumber : "<<rollnumber<<endl;
	cout<<"Marks : "<<marks<<endl;
	cout<<"***************************************"<<endl;
}

int Student::search(int temp)
{
	if(rollnumber == temp)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Student::getRollNumber()
{
	return rollnumber;
}


int getOption()
{
	int ch;

	cout<<"0. EXIT\n1. ACCEPT\n2. PRINT\n3. SEARCH\n4. SORT"<<endl;
	cin>>ch;

	return ch;
}

int comparator(void *a, void *b) {
    return (int)(((Student *)a)->getRollNumber() - ((Student *)b)->getRollNumber());
}


int main()
{
	Student s[2];
	int ch;
	int num;
	int flag = 0;

	

	

	while(ch=getOption())
	{
		switch(ch)
		{
			case ACCEPT:
					for(int i = 0;i<2;i++)
					{
						s[i].accept();
					}
					break;

			case PRINT:
					for(int i=0;i<2;i++)
					{
						s[i].print();
					}
					break;

			case SEARCH:
					cout<<"Enter Roll Number to be search"<<endl;
					cin>>num;

					for(int i=0;i<2;i++)
					{
						if(s[i].search(num))
						{
							s[i].print();
							flag = 1;
							break;
						}
					}

					if(flag == 0)
					{
						cout<<"RECORD NOT FOUND"<<endl;
					}
					break;

				case SORT:
					qsort(s, 2, sizeof(Student), (int(*)(const void*, const void*))comparator);
					cout<<"RECORD IS SORTED"<<endl;
					break;

		}
	}

}