#include "iostream"
#include "math.h"
using namespace std;

class NegativeNumber
{
public:
		void err()
		{
			cout<<"Number is Negative"<<endl;
		}
};

int sqr(int num)
{
	try
	{
		if(num<0)
		{
			throw(NegativeNumber());
		}
		else
		{
			cout<<"Square of Number : "<<num*num<<endl;
		}
	}
	catch(NegativeNumber e)
	{
		e.err();
	}
}

int main()
{
	int num;

	cout<<"Enter Number"<<endl;
	cin>>num;

	sqr(num);
	return 0;
}