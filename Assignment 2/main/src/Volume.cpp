#include "iostream"
#include "Volume.h"

using namespace std;

Volume::Volume()
{
	radius = 0;
	height = 0;
}

void Volume::getData()
{
	cout<<"Enter radius : ";
	cin>>radius;

	cout<<endl<<"Enter Height : ";
	cin>>height;
}

double Volume::display()
{
	return 3.14*radius*radius*height;
}

Volume::~Volume() {
	// TODO Auto-generated destructor stub
}

