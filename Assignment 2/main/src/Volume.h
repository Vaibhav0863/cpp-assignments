/*
 * Volume.h
 *
 *  Created on: 09-Mar-2020
 *      Author: vaibhav
 */

#ifndef VOLUME_H_
#define VOLUME_H_

class Volume
{
	int radius;
	int height;

public:
	Volume();
	void getData();
	double display();
	~Volume();
};

#endif /* VOLUME_H_ */
