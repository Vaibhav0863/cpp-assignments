#include "iostream"
#include "includes/header.h"
using namespace std;


Time :: Time()
{
	this->hh =0;
	this->mm = 0;
	this->ss = 0;
}

Time :: Time(int hh,int mm,int ss)
{
	this->hh = hh;
	this->mm = mm;
	this->ss = ss;
}

int Time ::  getHour()
{
	return this->hh;
}
int Time ::  getMinute()
{
	return this->mm;
}
int Time :: getSeconds()
{
	return this->ss;
}

void Time ::  setHour(int hh)
{
	this->hh = hh;
}
void Time :: setMinute(int mm)
{
	this->mm = mm;
}
void Time :: setSeconds(int ss)
{
	this->ss = ss;
}

void Time :: print_time()
{
	cout << this->getHour() << "-" << this->getMinute() << "-" << this->getSeconds()  <<endl;
}