#ifndef HEADER_H_
#define HEADER_H_

class Time
{
private:
	int hh;
	int mm;
	int ss;

public:
	// Parameter less and Parameterized constructor
	Time();
	Time(int hh,int mm,int ss);

	// Getter methods (Inspectors)
	int getHour();
	int getMinute();
	int getSeconds();

	// Setter methods (mutators)
	void setHour(int hh);
	void setMinute(int mm);
	void setSeconds(int ss);

	// Member function of class
	void print_time();

};

#endif