#include "iostream"
#include "functions.cpp"
#include "includes/header.h"
using namespace std;




int main()
{
	Time *t = new Time[2];
	int h,m,s;

	for(int i=0;i<2;i++)
	{
		cout<<"Enter hour"<<endl;
		cin>>h;
		t[i].setHour(h);

		cout<<"Enter minute"<<endl;
		cin>>m;
		t[i].setMinute(m);

		cout << "Enter seconds" << endl;
		cin >> s;
		t[i].setSeconds(s);
	}

	for(int i=0; i < 2 ; i++)
	{
		t[i].print_time();
	}

	delete []t;
	t=NULL;
}