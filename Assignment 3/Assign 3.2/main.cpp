#include "iostream"

using namespace std;

class Matrix
{
private:
	int row;
	int col;
	int **matx;

public:
	Matrix();
	Matrix(int row, int col);

	void getData();
	void Display();
};

Matrix :: Matrix()
{
	this->row = 0;
	this->col = 0;

	this->matx = new int*[this->row];

	for(int i=0 ; i<this->row; i++)
	{
		matx[i] = new int[this->col];
	}
}

Matrix :: Matrix(int row, int col)
{
	this->row = row;
	this->col = col;

	this->matx = new int*[this->row];

	for(int i=0 ; i<this->row; i++)
	{
		matx[i] = new int[this->col];
	}
}

void Matrix :: getData()
{
	int row = this->row;
	int col = this->col;

	cout<< "Enter "<< row*col << "matrix elements " << endl;

	for(int i=0;i<row;i++)
	{
		for(int j=0 ; j<col ; j++)
		{
			cin>>this->matx[i][j];
		}
	}
}

void Matrix :: Display()
{
	int row = this->row;
	int col = this->col;

	cout<< "Matrix Elements : " << endl;
	for(int i=0;i<row;i++)
	{
		for(int j=0 ; j<col ; j++)
		{
			cout<<this->matx[i][j]<< " " ;
		}
		cout<<endl;
	}
}

int main()
{
	Matrix m(2,2);

	m.getData();
	m.Display();
}