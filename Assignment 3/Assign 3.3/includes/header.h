#ifndef HEADER_H_
#define HEADER_H_

class Password
{
private:
	char password[20];

public:
	Password();

	void validator(const char *pass);
};

class PasswordMismatch
{
public:
	void err();
};

#endif