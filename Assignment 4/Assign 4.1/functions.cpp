#include "iostream"
#include "includes/account.h"
#include "includes/exceptions.h"

using namespace std;

Insufficient_fund :: Insufficient_fund(int accid, double cur_balance, double withdraw_amount)
{
	this->accid = accid;

	this->cur_balance = cur_balance;

	this->withdraw_amount = withdraw_amount;
}

void Insufficient_fund :: display()
{
	cout<< "\nInsufficient Fund\n" <<endl;
	cout<<"Hello " << this->accid <<", Your current account balance is " << this->cur_balance << " and your are trying to withdraw." << this->withdraw_amount<<endl;
}


Account :: Account()
{
	this->id = 0;
	this->type = SAVING;
	this->balance = 0;
}

Account :: Account(int id, enum account_type type)
{
	this->id = id;
	this->type = type;
	this->balance = balance;
}

void Account :: accept()
{
	int ch;
	cout<< "Enter Your Account ID : ";
	cin>>this->id;

	cout<<"Enter Account Type : " << endl;
	cout<< "1. Saving\n2. Current\n3. Demat"<<endl;
	cin>>ch;

	switch(ch)
	{
		case SAVING:
				this->type = SAVING;
				break;
		case CURRENT:
				this->type = CURRENT;
				break;
		case DMAT:
				this->type = DMAT;
				break;
		default:
				cout<<"\nInvalid Account Type"<<endl;
				break;
	}
}
void Account :: display()
{
	cout << "Account Information " << endl;
	cout<< "Account ID : " << this->id << endl;
	switch(this->type)
	{
		case SAVING:
					cout<< "Account Type : Saving"<<endl;
					break;
		case CURRENT:
					cout<< "Account Type : Current" <<endl;
					break;
		case DMAT:
					cout<< "Account Type : Demat" <<endl;
					break;
	}
	cout<< "Account Balance : " << this->balance << endl;
}

void Account :: set_id(int id)
{
	this->id = id;
}
void Account :: set_type(enum account_type type)
{
	this->type = type;
}

int Account :: get_id()
{
	return this->id;
}

enum account_type Account :: get_type()
{
	return this->type;
}

double Account :: get_balance()
{
	return this->balance;
}

void Account :: deposit(double amount)
{
	this->balance  = this->balance + amount;
}
void Account :: withdraw(double amount)
{
	try
	{
	if ((this->balance - amount ) < 0)
		throw Insufficient_fund(this->get_id(),this->get_balance(),amount);
	else
		this->balance = this->balance - amount;
	}
	catch(Insufficient_fund inf)
	{
		inf.display();
		exit(0);
	}

}

int getChoice()
{
	int ch;
	cout<< "0. EXIT\n1. DEPOSIT\n2. WITHDRAW"<<endl;
	cin>>ch;

	return ch;
}

Insufficient_fund::~Insufficient_fund()
{

}

Account :: ~Account()
{

}