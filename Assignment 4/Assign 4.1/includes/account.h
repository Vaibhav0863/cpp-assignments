#ifndef ACCOUNT_H_
#define ACCOUNT_H_

enum account_type{SAVING=1,CURRENT,DMAT};
enum option{EXIT,DEPOSIT,WITHDRAW};
int getChoice();

class Account
{
	int id;
	enum account_type type;
	double balance;

	public:

		Account();
		Account(int id, enum account_type type);

		void accept();
		void display();

		void set_id(int id);
		void set_type(enum account_type type);

		int get_id();
		enum account_type get_type();
		double get_balance();

		void deposit(double amount);
		void withdraw(double amount);

		~Account();
};

#endif