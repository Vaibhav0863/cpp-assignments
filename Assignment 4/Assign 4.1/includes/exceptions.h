#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

class Insufficient_fund
{
	int accid;
	double cur_balance;
	double withdraw_amount;

public:
	Insufficient_fund(int accid,double cur_balance, double withdraw_amount);

	void display();

	~Insufficient_fund();
};

#endif