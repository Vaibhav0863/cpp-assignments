#include "iostream"
// #include "headers/exceptionheader.h"
#include "includes/account.h"
#include "includes/exceptions.h"
#include "functions.cpp"

using namespace std;

// enum account_type{SAVING=1,CURRENT,DMAT};
// enum option{EXIT,DEPOSIT,WITHDRAW};


int main()
{
	Account acc[3];

	cout<< "Getting Account Details : "<< endl;

	for(int i=0 ; i< 3 ; i++)
	{
		acc[i].accept();
	}

	int ch;
	int acc_id;
	double bal;
	int flag = 0;
	while(ch=getChoice())
	{
		switch(ch)
		{
			case DEPOSIT:
										cout<< "Enter Your Account ID : ";
										cin>>acc_id;
										for(int i=0; i<3; i++)
										{
											if(acc[i].get_id() == acc_id)
											{
												flag = 1;
												cout<< "Enter Your Amount : ";
												cin>>bal;

												acc[i].deposit(bal);

												acc[i].display();
												break;
											}
										}
										if(flag == 0)
										{
											cout<< "Account Not Found" <<endl;
										}
										break;
			case WITHDRAW:
										cout<< "Enter Your Account ID : ";
										cin>>acc_id;
										for(int i=0; i<3; i++)
										{
											if(acc[i].get_id() == acc_id)
											{
												flag = 1;
												cout<< "Enter Your Amount : ";
												cin>>bal;

												acc[i].withdraw(bal);
												acc[i].display();
												break;
											}
										}
										if(flag == 0)
										{
											cout<< "Account Not Found" <<endl;
										}

		}
	}

}