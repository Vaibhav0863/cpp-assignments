#include "iostream"
#include "string.h"
#include "headers/headers.h"

using namespace std;

Address :: Address()
{
	strcpy(this->building,"");
	strcpy(this->street,"");
	strcpy(this->city,"");
	this->pin = 0;
	

}
Address :: Address(const char* building, const char* street, const char* city, int pin)
{
	strcpy(this->building,building);
	strcpy(this->street,street);
	strcpy(this->city,city);
	this->pin = pin;
}


void Address :: accept()
{
	char building[30];
	char street[30];
	char city[30];
	int pin;

	try
	{
		cout<<"Enter Building Name : ";
		cin>>building;
		this->set_building(building);
	
		cout<<"Enter Street Name : ";
		cin>> street;
		this->set_street(street);
	
		cout<< "Enter City Name : ";
		cin>> city;
		this->set_city(city);
	
		cout<< "Enter Pincode : ";
		cin>> pin;
		this->set_pin(pin);
	}
	catch(const char* msg)
	{
		cout<<msg<<endl;
	}

}

void Address :: display()
{
	cout<< "Building : "<<this->get_building()<<endl;
	cout<< "Street : "<<this->get_street()<<endl;
	cout<< "City : "<< this->get_city()<<endl;
	cout<< "Pincode : "<<this->get_pin()<<endl;
}

void Address :: set_building(const char* building)
{
	
		if(strlen(building)==1)
			throw ("Invalid Input");
	
		strcpy(this->building,building);
	
}
void Address :: set_street(const char* street)
{
	if(strlen(street)==1)
		throw "Invalid Input";

	strcpy(this->street,street);
}

void Address :: set_city(const char* city)
{
	if(strlen(city)==1)
		throw "Invalid Input";

	strcpy(this->city,city);
}

void Address :: set_pin(int pin)
{
	if(pin==0)
		throw "Invalid Input";
	this->pin = pin;
}

char* Address :: get_building()
{
	return this->building;
}

char* Address :: get_street()
{
	return this->street;
}

char* Address ::  get_city()
{
	return this->city;
}

int Address :: get_pin()
{
	return this->pin;
}