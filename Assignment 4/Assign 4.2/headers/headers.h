#ifndef HEADER_H_
#define HEADER_H_


class Address
{
	char building[30];
	char street[30];
	char city[30];
	int pin;

public:
		Address();
		Address(const char* building, const char* street, const char* city, int pin);

		void accept();
		void display();

		// getters and setters

		char* get_building();
		char* get_street();
		char* get_city();
		int get_pin();

		void set_building(const char* building);
		void set_street(const char* street);
		void set_city(const char* city);
		void set_pin(int pin);
		
};

#endif