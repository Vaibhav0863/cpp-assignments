#include "stdio.h"

enum option{EXIT,INIT,ACCEPT,PRINT};

struct date
{
	int dd;
	int mm;
	int yy;
};


void initDate(struct date *d)
{
	d->dd = 03;
	d->mm = 03;
	d->yy = 2020;
}

void AcceptDateFromConsole(struct date* ptDate)
{
	// cout<<"Enter Day"<<endl;
	printf("ENTER DAY\n");
	// cin>>ptDate->dd;
	scanf("%d",&ptDate->dd);

	// cout<<"Enter Month"<<endl;
	printf("ENTER MONTH\n");
	// cin>>ptDate->mm;
	scanf("%d",&ptDate->mm);


	// cout<<"Enter Year"<<endl;
	printf("ENTER YEAR\n");
	// cin>>ptDate->yy;
	scanf("%d",&ptDate->yy);
	
}

void PrintDateOnConsole(struct date* ptDate)
{
	// cout<<ptDate->dd<<"/"<<ptDate->mm<<"/"<<ptDate->yy<<endl;
	printf("%d/",ptDate->dd);
	printf("%d/",ptDate->mm);
	printf("%d",ptDate->yy);
	printf("\n");

}

int getOption()
{
	int ch;
	printf("0. EXIT\n1. INIT\n2. ACCEPT\n3. PRINT\n");
	scanf("%d",&ch);

	return ch;
}

int main()
{
	struct date d;
	int ch;

	while(ch = getOption())
	{
		switch(ch)
		{
			case 1:
					initDate(&d);
					break;
			case 2:
					AcceptDateFromConsole(&d);
					break;
			case 3:
					PrintDateOnConsole(&d);
					break;
		}
	}
}