#include "iostream"
using namespace std;

enum option{EXIT,INIT,ACCEPT,PRINT,ISLEAP};


struct date
{
	int dd;
	int mm;
	int yy;

	void initDate(struct date *d)
	{
		d->dd = 03;
		d->mm = 03;
		d->yy = 2020;
	};

	void AcceptDateFromConsole(struct date* ptDate)
	{
		cout<<"Enter Day"<<endl;
		cin>>ptDate->dd;

		cout<<"Enter Month"<<endl;
		cin>>ptDate->mm;
		
		cout<<"Enter Year"<<endl;
		cin>>ptDate->yy;	
	};

	void PrintDateOnConsole(struct date* ptDate)
	{
		cout<<ptDate->dd<<"/"<<ptDate->mm<<"/"<<ptDate->yy<<endl;
	};

	bool isLeapYear(struct date d)
	{
		if(d.yy%4 == 0)
			return true;

		if(d.yy%100 == 0)
			return true;

		if(d.yy%400 == 0)
			return true;

		return false;
	}
};

int getOption()
{
	int ch;
	// printf("0. EXIT\n1. INIT\n2. ACCEPT\n3. PRINT\n");
	cout<<"0. EXIT\n1. INIT\n2. ACCEPT\n3. PRINT\n4. IS LEAP YEAR\n";
	cout<<"ENTER CHOICE"<<endl;
	// scanf("%d",&ch);
	cin>>ch;

	return ch;
}

int main()
{
	date d;
	int ch;
	while(ch = getOption())
	{
		switch(ch)
		{
			case 1:
					d.initDate(&d);
					break;
			case 2:
					d.AcceptDateFromConsole(&d);
					break;
			case 3:
					d.PrintDateOnConsole(&d);
					break;
			case 4:
					cout<<(d.isLeapYear(d)?"LEAP YEAR":"NOT A LEAP YEAR")<<endl;
					// cout<<d.isLeapYear(d)<<endl;
					break;
		}
	}
}


