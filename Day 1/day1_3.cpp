#include "iostream"

using namespace std;

enum option{EXIT,ACCEPT,PRINT,ISLEAP};

class Date
{
private:
	int dd;
	int mm;
	int yy;
public:
	Date();

	Date(int d,int m,int y);

	void PrintDateOnConsole();

	void AcceptDateFromConsole();

	bool IsLeapYear();


};

Date::Date()
{
		dd = 00;
		mm = 00;
		yy = 0000;
}

Date::Date(int d,int m,int y)
{
	dd = d;
	mm = m;
	yy = y;
}

void Date::AcceptDateFromConsole()
{
	cout<<"Enter Day"<<endl;
	cin>>dd;

	cout<<"Enter Month"<<endl;
	cin>>mm;

	cout<<"Enter Year"<<endl;
	cin>>yy;
}

void Date::PrintDateOnConsole()
{
	cout<<dd<<"/"<<mm<<"/"<<yy<<endl;
}

bool Date::IsLeapYear()
{
	if(yy%4==0 || yy%100==0 || yy%400==0)
		return true;

	return false;
}

int getOption()
{
	int ch;
	// printf("0. EXIT\n1. INIT\n2. ACCEPT\n3. PRINT\n");
	cout<<"0. EXIT\n1. ACCEPT\n2. PRINT\n3. IS LEAP YEAR\n";
	cout<<"ENTER CHOICE"<<endl;
	// scanf("%d",&ch);
	cin>>ch;

	return ch;
}
int main()
{
Date d;

int ch;

while(ch=getOption())
{
	switch(ch)
	{
		case ACCEPT:
				d.AcceptDateFromConsole();
				break;
		case PRINT:
				d.PrintDateOnConsole();
				break;
		case ISLEAP:
				cout<<(d.IsLeapYear()?"LEAP YEAR":"NOT A LEAP YEAR")<<endl;
				break;
	}
}

}